#!/bin/bash


export TZ="America/New_York"

curl --silent --request GET --form "token=$CI_JOB_TOKEN" --form ref=main --form per_page=115 --form page=2 https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs > process.json

curl --silent --request GET --form "token=$CI_JOB_TOKEN" --form ref=main --form per_page=115 --form page=1 https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs > process2.json


echo

jq -s 'flatten | sort_by(.started_at)' process.json process2.json | jq -r '.[] | "\(.name),\(.started_at),\(.finished_at),\(.duration)"' | while IFS=',' read -r name start_time end_time duration; do
    start_timestamp=$(date -u -d "$start_time" +"%s")
    end_timestamp=$(date -u -d "$end_time" +"%s")
    duration_seconds=$(echo "$duration" | awk -F: '{print ($1 * 3600) + ($2 * 60) + $3}')
    echo "$name,$start_timestamp,$end_timestamp,$duration_seconds"
done > jobs.txt

sort -t',' -k2,2 jobs.txt > sorted_jobs.txt

cat sorted_jobs.txt







