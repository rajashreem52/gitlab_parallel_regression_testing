#!/bin/bash

cd /gitlab_parallel_regression_testing

my_array=( $(bash gitlab-ci/get_failed_jobs.sh) )

cd tests

failed_tests=()  # Array to store failed test names

for testname in "${my_array[@]}"
do
  if ./runtests "${testname}"; then
    tar -cf "${testname}_$(date +"%Y%m%d%H%M%S").tar" "${testname}"
  else
    failed_tests+=("${testname}")
    tar -cf "${testname}_$(date +"%Y%m%d%H%M%S").tar" "${testname}"
  fi
done

# Create a failed test list
if [ ${#failed_tests[@]} -gt 0 ]; then
  echo "Failed tests:"
  for failed_test in "${failed_tests[@]}"
  do
    echo "${failed_test}"
  done
fi


  
