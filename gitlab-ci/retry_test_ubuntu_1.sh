#!/bin/bash

echo "RETRY=" $RETRY

if [ "$RETRY" == "FALSE" ] ;
then
 echo "job_exit:
   tags:
   - doc
   script:
    - echo exit_job
    ">>retry_test_ubuntu_1.yml

fi



if [ "$RETRY" == "TRUE" ] ;
then

my_array=( $(bash get_failed_jobs.sh) )



for testname in "${my_array[@]}"
do
   echo "

stages: 
  - parallel_retry_test
  - serial_test

job_${testname}:
   stage: parallel_retry_test
   tags:
   - doc
   timeout: 15 minutes
   script:
    - cd /gitlab_parallel_regression_testing/tests
    - ./runtests ${testname}
    - tar -cf ${testname}_$(date +\"%Y%m%d%H%M%S\").tar ${testname}
    
job_res:
   stage: serial_test
   when: always
   tags: 
     - doc
   script:
     - bash gitlab-ci/retry_test_ubuntu_2.sh
    ">>retry_test_ubuntu_1.yml

    
  
done



fi

