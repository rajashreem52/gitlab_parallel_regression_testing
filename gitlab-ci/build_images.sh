#!/bin/bash

if ([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "ALL" ])||([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "UBUNTU" ]) || [ "$RUNNERTYPE" == "ALL" ] ;
then


docker pull ubuntu:20.04

echo "FROM ubuntu:20.04 AS image

RUN apt-get -y update
RUN apt-get -y install sudo

RUN apt-get -y install wget

RUN sudo apt install build-essential -y

RUN sudo apt install curl -y

RUN apt-get install -y git

RUN git clone https://gitlab.com/rajashreem52/gitlab_parallel_regression_testing.git


WORKDIR /gitlab_parallel_regression_testing


RUN mkdir m4

RUN apt-get -y install libtool

RUN autoreconf -i


RUN ./configure

RUN make

RUN make buildcheck

RUN sudo ldconfig

RUN rm killm

RUN ln -s /bin/true killm

WORKDIR /gitlab_parallel_regression_testing/tests">Dockerfile

echo "RUN printf \"#!/bin/sh\n \
 find .  \( -type f -o -type l \) -a -name 'dotest' | grep -v "\.libs/" | xargs -L 1 dirname | cut -d / -f 1- | sort\n\">dockerlisttest.sh">>Dockerfile

docker build -t my-docker-image .

fi

if ([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "ALL" ])||([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "ORACLE_LINUX" ]) || [ "$RUNNERTYPE" == "ALL" ] ;
then

echo "FROM oraclelinux:8 AS image

RUN yum -y update

RUN yum -y install sudo

RUN yum -y install wget

RUN yum -y install curl

RUN sudo yum groupinstall \"Development Tools\" -y

RUN yum -y install git

RUN git clone https://gitlab.com/rajashreem52/gitlab_parallel_regression_testing.git

WORKDIR /gitlab_parallel_regression_testing

RUN mkdir m4

RUN yum -y install libtool

RUN autoreconf -i


RUN ./configure

RUN make

RUN make buildcheck

RUN sudo ldconfig

RUN rm killm

RUN ln -s /bin/true killm

WORKDIR /gitlab_parallel_regression_testing/tests">Dockerfile

echo "RUN printf \"#!/bin/sh\n \
 find .  \( -type f -o -type l \) -a -name 'dotest' | grep -v "\.libs/" | xargs -L 1 dirname | cut -d / -f 1- | sort\n\">dockerlisttest.sh">>Dockerfile

docker build -t my-oracle-linux .

fi
