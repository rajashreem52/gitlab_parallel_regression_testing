#!/bin/bash

imagename="my-docker-image"

i=1

if  [ "$RUNNERTYPE" == "WSL2" ] ||([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "ORACLE_LINUX" ])||[ "$RUNNERTYPE" == "MACOS" ] ; 
then
 echo "job_exit:
   tags:
   - doc
   script:
    - echo \"This is a exit job\"
    ">>runtest_ubuntu.yml
fi

if ([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "ALL" ])||([ "$RUNNERTYPE" == "DOCKER" ] && [ "$IMAGETYPE" == "UBUNTU" ]) || [ "$RUNNERTYPE" == "ALL" ] ;
then

docker stop new18
docker rm new18

array=( $(docker run -v ~/newlogs2:/linking  --name=new18 my-docker-image /bin/bash dockerlisttest.sh linking| tr -d '\r') )

arr=()

idx=0


for element in "${array[@]}"

do

  length=${#element}
  testname=${element:2:$((length-1))}
  arr[idx]=$testname
  idx=$((idx + 1))
done

for testname in "${arr[@]}"
do
   echo "
stages:
  - deploy
  - notify
  - retry

job_${testname}:
   stage: deploy
   tags:
   - doc
   timeout: 16 minutes
   script:
    - cd /gitlab_parallel_regression_testing/tests
    - ./runtests ${testname} 
    - if [ $? -eq 0 ]; then tar -cf ${testname}_$(date +\"%Y%m%d%H%M%S\").tar ${testname}; else tar -cf ${testname}_$(date +\"%Y%m%d%H%M%S\").tar ${testname}; exit 1; fi
job_result: 
  stage: notify
  script:
    - cd gitlab-ci
    - bash retry_test_ubuntu_1.sh 
  when: always
  artifacts:
   paths:
      -   gitlab-ci/retry_test_ubuntu_1.yml
retry_test:
  stage: retry
  when: always
  trigger:
    include: 
     - artifact: gitlab-ci/retry_test_ubuntu_1.yml
       job: job_result
    ">>runtest_ubuntu.yml

  i=$((i + 1))
  
done

fi
