#!/bin/bash


if  [ "$RUNNERTYPE" == "WSL2" ] || [ "$RUNNERTYPE" == "ALL" ] ; 
then

cd ~ 

git clone https://gitlab.com/rajashreem52/gitlab_parallel_regression_testing.git

cd gitlab_parallel_regression_testing

mkdir m4

apt-get install libtool

automake --add-missing 

autoreconf -i

./configure

make

cd tests

 

 arr=( $(find . \( -type f -o -type l \) -a -name 'dotest' | grep -v "\.libs/" | xargs -L 1 dirname | cut -d / -f 1- | sort | tr -d '\r') )

array=()

idx=0

for element in "${arr[@]}"

do

  length=${#element}
  testname=${element:2:$((length-1))}
  array[idx]=$testname
  idx=$((idx + 1))
done



passed=()

idx_pass=0

failed=()

idx_fail=0

for testname in "${array[@]}"
do
  ./runtests ${testname}
    EXITVAL=$?
    if [[ $EXITVAL == 1 ]]; then
        failed[idx_fail]=$testname
        idx_fail=$((idx_fail + 1))
    else
        passed[idx_pass]=$testname
        idx_pass=$((idx_pass + 1)) 
    fi
done

echo "PASSED/SKIPPED: $idx_pass"
for testname in "${passed[@]}"
do
  echo $testname
  echo
done

echo "FAILED:$idx_fail"
for testname in "${failed[@]}"
do
  echo $testname
  echo
done

fi
